#include "image.h"
#include "bmp_manager.h"
#include "main_content.h"
#include "rotate.h"
#include <stdio.h>
#include <stdlib.h>

int work(char* input_file, char* output_file) {
    struct image old_img;
    
    FILE *input_f= fopen(input_file, "rb");
 


    if (!input_f){
        fprintf(stderr, "Can't open input file\n");
        fclose(input_f);
        i_free(&old_img);
        return 1;
    }

       FILE *output_f=fopen(output_file, "wb");
    
    if (!output_f){
        fprintf(stderr, "Can't create/understand your output file\n");
        fclose(input_f); 
        fclose(output_f);
        i_free(&old_img);
        return 1;
    }

    //rotation
    if (from_bmp(input_f,&old_img)!=READ_OK){
        fprintf(stderr, "I can't work with your BMP :(\n Try another one \n");
        fclose(input_f); 
        fclose(output_f);
        i_free(&old_img);
        return 1;
        
    }

    fprintf(stdout, "Your BMP is in the work... \n");

    struct image new_img = rotate_image(&old_img);

    i_free(&old_img);

    to_bmp(output_f,&new_img);

    i_free(&new_img);

    if (to_bmp(input_f,&old_img)!=READ_OK){
        fprintf(stderr,"Error");
        fclose(input_f);
        fclose(output_f);
        return 1;
    }

    fclose(input_f); //now we can close the original file
    fclose(output_f);

    fprintf(stdout, "Done! \n");
    return 0;

}

