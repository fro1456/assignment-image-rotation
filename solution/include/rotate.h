//
// Created by Aleksey Ryabikin on 24.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#include "image.h"
struct image rotate_image(struct image* src_file);
#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
