//
// Created by Aleksey Ryabikin on 23.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_BMP_INF_H
#define ASSIGNMENT_IMAGE_ROTATION_BMP_INF_H
#include "image.h"
#include  <stdint.h>
#include <stdio.h>


struct bmp_header {
uint16_t bf_Type;
uint32_t  b_fileSize;
uint32_t bf_Reserved;
uint32_t b_OffBits;
uint32_t bi_Size;
uint32_t biWidth;
uint32_t  biHeight;
uint16_t  bi_Planes;
uint16_t bi_BitCount;
uint32_t bi_Compression;
uint32_t bi_SizeImage;
uint32_t bi_XPelsPerMeter;
uint32_t bi_YPelsPerMeter;
uint32_t bi_ClrUsed;
uint32_t  bi_ClrImportant;
} __attribute__((packed));
/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_EMPTY ,
};

enum read_status from_bmp( FILE* file, struct image *img );

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRONG_FILE,
};

enum write_status to_bmp( FILE* out, struct image const* img );




#endif //ASSIGNMENT_IMAGE_ROTATION_BMP_INF_H
