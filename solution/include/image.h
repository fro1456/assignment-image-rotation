//
// Created by Aleksey Ryabikin on 23.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>

struct image{
    uint64_t width, height;
    struct pixel* data;
};
struct pixel { uint8_t b, g, r; };
struct image i_create(uint64_t width, uint64_t height);
void i_free(struct image* image);
#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
