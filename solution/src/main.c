#include "image.h"
#include "bmp_manager.h"
#include "main_content.h"
#include "rotate.h"

#include <stdio.h>
#include <stdlib.h>

int main( int argc, char** argv ) {

    if (argc!=3){
        fprintf(stderr, "Wrong number of arguments\nEnter name of initial and new files\n");
        return 1;
    }
    
    return work(argv[1],argv[2]);
    
}
