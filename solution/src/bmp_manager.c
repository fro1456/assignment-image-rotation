//
// Created by Aleksey Ryabikin on 24.12.2021.
//
#include "bmp_manager.h"
#include <stdlib.h>

#define HEADER_SIZE 40;
#define BOFFBITS sizeof(struct bmp_header);
#define SIGNATURE 0x4d42;



static enum read_status read_header(FILE *file, struct bmp_header *header) {
    fread(header, sizeof(struct bmp_header), 1, file);
    return READ_OK;
}


static inline uint8_t padding_size(uint32_t width) {
    return width % 4;
}


static enum read_status conv_to_pxls(FILE *file, struct image *img) {

    uint8_t padding = padding_size(img->width);

    for(size_t i=0;i<img->height;i++){
        struct pixel* pxls= (img->data+i*(img->width));

        size_t res1 = fread(pxls, (size_t) img->width*sizeof(struct pixel),1,file);

        size_t res2 = fseek(file,padding, SEEK_CUR)!=0;

        if ((res1 != 1)||(res2!=0)){
            fprintf(stderr,"LINE IS EMPTY\n");
            return READ_EMPTY;
        }

    }
    return READ_OK;
}


enum read_status from_bmp(FILE *file, struct image *img){
    struct bmp_header bmp_header = {0};

    if (file == NULL || img == NULL){
        return READ_EMPTY;
    }

    enum read_status status = read_header(file, &bmp_header);

    img->width = bmp_header.biWidth;
    img->height = bmp_header.biHeight;
    img->data = malloc(sizeof(struct pixel) * img->width * img->height);

    conv_to_pxls(file,img);

    return status!=0?status:READ_OK;
}


void write_bmp_header(FILE *out, uint64_t width, uint64_t height) {

    struct bmp_header bmpHeader;
    bmpHeader.biWidth = width;
    bmpHeader.biHeight = height;
    bmpHeader.bf_Type = SIGNATURE;
    bmpHeader.b_fileSize = height * width * 3 + BOFFBITS;
    bmpHeader.bf_Reserved = 0;
    bmpHeader.b_OffBits = BOFFBITS;
    bmpHeader.bi_Size = HEADER_SIZE;
    bmpHeader.bi_Planes = 1;
    bmpHeader.bi_BitCount = 24;
    bmpHeader.bi_Compression = 0;
    bmpHeader.bi_SizeImage = height * width * 3;
    bmpHeader.bi_XPelsPerMeter = 0;
    bmpHeader.bi_YPelsPerMeter = 0;
    bmpHeader.bi_ClrUsed = 0;
    bmpHeader.bi_ClrImportant = 0;

    fwrite(&bmpHeader, sizeof(struct bmp_header), 1, out);
}


enum write_status to_bmp(FILE *out, struct image const *img) {

    if (!out) {
        return WRONG_FILE;
    }

    write_bmp_header(out, img->width, img->height);

    uint32_t padding_data = 0;
    uint32_t offset = padding_size(img->width);

    for (uint32_t i = 0; i < img->height; i++) {
        fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        fwrite(&padding_data, 1, offset, out);
    }
    return WRITE_OK;
}
