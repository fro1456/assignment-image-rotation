//
// Created by Aleksey Ryabikin on 24.12.2021.
//
#include "image.h"

struct image i_create(uint64_t width, uint64_t height){
    struct image image = {0};
    image.width = width;
    image.height = height;
    image.data = malloc(sizeof(struct pixel) * image.width * image.height);
    return image;

}
void i_free(struct image* image){
    free(image->data);
}
