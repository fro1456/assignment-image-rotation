//
// Created by Aleksey Ryabikin on 24.12.2021.
//
#include "rotate.h"
#include <stdlib.h>

struct image rotate_image(struct image* src_file){
    if (src_file){

        uint32_t width = src_file->width;
        uint32_t height = src_file->height;

        struct image rotated = i_create(height, width);

        for (size_t i = 0; i < width; i++) {
            for (size_t j = 0; j < height; j++) {
                struct pixel pixel = src_file->data[(rotated.width - j - 1) * width + i];
                rotated.data[i * height + j] = pixel;
            }
        }

        return rotated;
    }
    else{
        return (struct image){.width=0, .height=0, .data=NULL};
    }
}

